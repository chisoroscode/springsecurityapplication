package com.chisoroscode.springsecurityapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityApplicationApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityApplicationApplication.class, args);
    }

}
