package com.chisoroscode.springsecurityapplication.repository;

import com.chisoroscode.springsecurityapplication.security.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface Userrepository extends JpaRepository<User,Integer> {
    Optional<User> findByEmail(String email);
}
