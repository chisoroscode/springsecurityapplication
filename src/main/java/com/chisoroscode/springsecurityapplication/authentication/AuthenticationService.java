package com.chisoroscode.springsecurityapplication.authentication;

import com.chisoroscode.springsecurityapplication.configuration.JwtService;
import com.chisoroscode.springsecurityapplication.repository.Userrepository;
import com.chisoroscode.springsecurityapplication.security.User;
import com.chisoroscode.springsecurityapplication.security.utils.Role;
import com.chisoroscode.springsecurityapplication.tokenmanager.Token;
import com.chisoroscode.springsecurityapplication.tokenmanager.TokenType;
import com.chisoroscode.springsecurityapplication.tokenmanager.Tokenrepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final Userrepository userrepository;
    private final Tokenrepository tokenrepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;


    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getEmail(),
                authenticationRequest.getPassword()
        ));
        var user=userrepository.findByEmail(authenticationRequest.getEmail()).orElseThrow();
        var jwtToken=jwtService.generateToken(user);
        var refreshToken=jwtService.generateRefreshToken(user);
        invalidateAndRevokeToken(user);
        saveBearerToken(user,jwtToken);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();

    }

    public AuthenticationResponse register(RegisterRequest registerRequest) {
        var user= User.builder()
                .firstName(registerRequest.getFirstname())
                .lastName(registerRequest.getLastname())
                .email(registerRequest.getEmail())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(Role.USER)
                .build();
        userrepository.save(user);
        var jwtToken=jwtService.generateToken(user);
        var refreshToken=jwtService.generateRefreshToken(user);

        saveBearerToken(user, jwtToken);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }

    private void saveBearerToken(User user, String jwtToken) {
        var token= Token.builder()
                .token(jwtToken)
                .expired(false)
                .tokenType(TokenType.BEARER)
                .user(user)
                .revoked(false)
                .build();
        tokenrepository.save(token);
    }

    private void invalidateAndRevokeToken(User user){
        var validTokens=tokenrepository.findAllValidTokenByUser(user.getId());
        validTokens.forEach(
                (token) -> {
                    token.setRevoked(true);
                    token.setExpired(true);
                }
        );
        tokenrepository.saveAll(validTokens);
    }


    public void refreshToken(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

        final String authHeader=httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;

        if (authHeader==null||!authHeader.startsWith("Bearer")){
            return;
        }
        refreshToken=authHeader.substring(7);
        userEmail=jwtService.extractUsername(refreshToken);
        if (userEmail!=null){
            var userDetails=this.userrepository.findByEmail(userEmail).orElseThrow();

            if (jwtService.isTokenValid(refreshToken,userDetails)){
              var accessToken=jwtService.generateToken(userDetails);
              var authResponse=AuthenticationResponse.builder()
                      .refreshToken(refreshToken)
                      .accessToken(accessToken)
                      .build();
                new ObjectMapper().writeValue(httpServletResponse.getOutputStream(),authResponse);
            }
        }
    }
}
