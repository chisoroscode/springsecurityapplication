package com.chisoroscode.springsecurityapplication.configuration;

import com.chisoroscode.springsecurityapplication.tokenmanager.Tokenrepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {
    private final Tokenrepository tokenrepository;
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        final String authHeader=request.getHeader("Authorization");
        final String jwt;
        if (authHeader==null||!authHeader.startsWith("Bearer")){
            return;
        }

        jwt=authHeader.substring(7);
        var storedToken=tokenrepository.findByToken(jwt);
        if (storedToken.isPresent()){
            var token=storedToken.get();
            token.setExpired(true);
            token.setRevoked(true);
            tokenrepository.save(token);
        }
    }
}
